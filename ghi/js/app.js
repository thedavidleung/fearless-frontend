function createCard(name, description, pictureUrl, start, end, subtitle) {
    return `
      <div class="card mb-2 me-2 shadow-lg p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${start} - ${end}
        </div>
        </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {

        // Figure out what to do when the response is bad

      } else {

        //Note that despite the method being named json(),
        //the result is not JSON but is instead the result of taking JSON
        //as input and parsing it to produce a JavaScript object

        const data = await response.json();
        let i =0;
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const starts = details.conference.starts;
              const start = new Date(starts).toLocaleDateString();
              const ends = details.conference.ends;
              const end = new Date(ends).toLocaleDateString();
              const subtitle = details.conference.location.name;
              const html = createCard(name, description, pictureUrl, start, end, subtitle);
              const columns = document.querySelectorAll('.col');
              columns[i % 3].innerHTML += html;
            }
            if (i > 2){
              i = 0;
            }
            i++;
          }
        }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e);

    }

});
